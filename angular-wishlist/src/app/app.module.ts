import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
Import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { ViajeComponent } from './Destino/viaje/viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';

const routes: Routes = [
{ path: '', redirectTo: 'home', pathMatch: 'full'},
{ path: 'home', component: ListaDestinosComponent},
{ path: 'destino', component:  DestinoDetalleComponent}  

];

@NgModule({
  declarations: [
    AppComponent,
    ViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
